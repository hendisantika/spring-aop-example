# Spring AOP Example

### Advice
##### @Around
It is an advice that can include all the exits that the method of the target object is invoked through the proxy.
```
    @Around("helloPointCut()")
    public Object helloAround(ProceedingJoinPoint pjp) throws Throwable {
        logger.info("Around");
        logger.info("narut0");
        Object ret = pjp.proceed();
        logger.info("Kakashi");
        return ret;
    }
```

#### @Before
As the name implies, it is an advice used before the object's methods are executed.
```
    @Before("helloPointCut()")
    public void logJoinPoint(JoinPoint jp) {
        logger.info("Before");
        logger.info(jp.getSignature().getDeclaringTypeName());
        logger.info(jp.getSignature().getName());
        for(Object arg : jp.getArgs()) {
            logger.info(arg + "");
        }
    }
```

#### @AfterReturning
This method is executed after the method has finished executing. However, it is only executed when the exception is not occurred and it is normally executed. You can refer to the return value in that method, but you can not change the value. However, if the return value is a reference type, you can manipulate the object.
```
    @AfterReturning(pointcut="helloPointCut()", returning="name")
    public void logReturnValue(String name) {
        logger.info("AfterReturning");
        logger.info(name);
    }
```

#### @AfterThrowing
It is a method that is executed when an exception is raised in a method. An exception can be passed through the property using throwing . It is executed only when the type specified by throwing occurs.
```
    @AfterThrowing(pointcut = "helloPointCut()", throwing = "e")
    public void logThrowValue(UserException e){
        logger.info("exception");
        logger.info(e.getId());
    }
```

#### @After
It is executed both when the method is normal and when an exception occurs. Think of it as a finally .
```
    @After("helloPointCut()")
    public void logAfterValue(JoinPoint jp) {
        logger.info("after");
        logger.info(jp.getSignature().getDeclaringTypeName());
        logger.info(jp.getSignature().getName());
        for(Object arg : jp.getArgs()) {
            logger.info(arg + "");
        }
    }
```

#### @Configurable
Let's say you have a User domain. The domain is a common ORM or MVC domain. If business logic enters the domain, you will need to get a DI.

```
public class User {

    @Autowired
    private HelloAopService helloAopService;

    public String getName(){
        return helloAopService.getName();
    }
}
```

User is not registered as a bean. Typically, you use a domain like this.

`User user = new User();`

This will of course **result in a NullPointerException** when calling getName on the **helloAopService** with a value of **null**.

You can use **@Configurable** in a simple way .
```
@Configurable(preConstruction = true)
public class User {

    @Autowired
    private HelloAopService helloAopService;

    public String getName(){
        return helloAopService.getName();
    }

}
```
Note: You may need to configure it `@EnableSpringConfigured` or `<context:spring-configured />` set it.