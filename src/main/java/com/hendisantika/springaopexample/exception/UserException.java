package com.hendisantika.springaopexample.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-aop-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/07/18
 * Time: 07.04
 * To change this template use File | Settings | File Templates.
 */
public class UserException extends RuntimeException {

    private String id;

    public UserException(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}