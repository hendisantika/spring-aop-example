package com.hendisantika.springaopexample.service;

import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-aop-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/07/18
 * Time: 07.02
 * To change this template use File | Settings | File Templates.
 */
@Component
public class HelloAopService {

    public String name() {
        return "hello";
    }

    public String getName() {
//        if(true){
//            throw new UserException("wonwoo");
//        }
        return "wonwoo";
    }
}