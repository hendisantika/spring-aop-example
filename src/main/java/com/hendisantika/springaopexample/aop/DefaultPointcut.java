package com.hendisantika.springaopexample.aop;

import org.springframework.aop.ClassFilter;
import org.springframework.aop.MethodMatcher;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.annotation.AnnotationMethodMatcher;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-aop-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/07/18
 * Time: 07.08
 * To change this template use File | Settings | File Templates.
 */
public class DefaultPointcut implements Pointcut {

    @Override
    public ClassFilter getClassFilter() {
        return (clazz) -> true;
    }

    @Override
    public MethodMatcher getMethodMatcher() {
        return new AnnotationMethodMatcher(AopAnnotation.class);
    }
}