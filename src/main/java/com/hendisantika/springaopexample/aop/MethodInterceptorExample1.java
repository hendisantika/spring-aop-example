package com.hendisantika.springaopexample.aop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-aop-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/07/18
 * Time: 07.09
 * To change this template use File | Settings | File Templates.
 */
public class MethodInterceptorExample1 implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {

        System.out.println("method interceptor start");
        Object returnValue = methodInvocation.proceed();

        System.out.println("method interceptor end");
        return returnValue;
    }
}