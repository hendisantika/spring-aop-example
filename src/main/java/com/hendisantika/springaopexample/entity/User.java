package com.hendisantika.springaopexample.entity;

import com.hendisantika.springaopexample.service.HelloAopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-aop-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/07/18
 * Time: 07.02
 * To change this template use File | Settings | File Templates.
 */
@Configurable(preConstruction = true)
public class User {

    @Autowired
    private HelloAopService helloAopService;

    public String getName() {
        return helloAopService.getName();
    }

}