package com.hendisantika.springaopexample.component;

import com.hendisantika.springaopexample.aop.AopAnnotation;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-aop-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/07/18
 * Time: 07.05
 * To change this template use File | Settings | File Templates.
 */
@Component
public class HelloComponent {

    @AopAnnotation
    public String getComponent() {
        String hello = "hello";
        System.out.println(hello);
        return hello + " naruto";
    }

    public String getComponent1() {
        String hello = "hello";
        System.out.println(hello);
        return hello + " naruto1";
    }
}