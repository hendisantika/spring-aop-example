package com.hendisantika.springaopexample;

import com.hendisantika.springaopexample.aop.DefaultPointcut;
import com.hendisantika.springaopexample.aop.MethodInterceptorExample1;
import org.springframework.aop.PointcutAdvisor;
import org.springframework.aop.support.DefaultBeanFactoryPointcutAdvisor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;

@SpringBootApplication
@EnableSpringConfigured
public class SpringAopExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringAopExampleApplication.class, args);
    }

    @Bean
    public PointcutAdvisor potincutAdviser() {
        DefaultBeanFactoryPointcutAdvisor advisor = new DefaultBeanFactoryPointcutAdvisor();
        advisor.setPointcut(new DefaultPointcut());
        advisor.setAdvice(new MethodInterceptorExample1());
        return advisor;
    }
}
