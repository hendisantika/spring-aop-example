package com.hendisantika.springaopexample.aop;

import com.hendisantika.springaopexample.component.HelloComponent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-aop-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/07/18
 * Time: 16.47
 * To change this template use File | Settings | File Templates.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MethodInterceptorExample1Test {

    @Autowired
    private HelloComponent helloComponent;

    @Test
    public void method() {
        System.out.println(helloComponent.getComponent());
        System.out.println(helloComponent.getComponent1());
    }

}