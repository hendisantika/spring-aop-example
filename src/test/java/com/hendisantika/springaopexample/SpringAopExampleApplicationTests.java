package com.hendisantika.springaopexample;

import com.hendisantika.springaopexample.entity.User;
import com.hendisantika.springaopexample.service.HelloAopService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringAopExampleApplicationTests {
    @Autowired
    private HelloAopService helloAopService;

    @Test
    public void contextLoads() {
//		helloAopService.getName();
        User user = new User();
        String name = user.getName();
        System.out.println(name);
    }

}
